/*
 * event2
 * Copyright (c) 2020 Deogenes (https://github.com/Deogenes/event2)
 * MIT Licensed
 */

"use strict";

const amqp = require("amqplib");
const crypto = require('crypto');

module.exports = {

	name: "event2",

	/**
	 * Default settings
	 */
	settings: {
		amqpUrl: null,
		exchange: 'eventos',
		deadExchange: 'eventosDead',
		timeRetrive: 60000,
		deduplicationTTL: 1800000, //30m
		//deduplicationTTL: 10000,
		serviceName: null,
		//exchangeType: 'x-message-deduplication',
		exchangeType: 'direct'
	},

	/**
	 * Actions
	 */
	actions: {
		test(ctx) {
			return "Hello " + (ctx.params.name || "Anonymous");
		}
	},

	/**
	 * Methods
	 */
	methods: {
		emit2(ctx, event, body){
			(async () => {

				let data = {
					params: body
				}

				let jsonData = JSON.stringify(data)

				let args = new Object()
				let headers = new Object();
				
				let hash = crypto.createHash('sha256');
				headers['x-deduplication-header'] = hash.update(jsonData).digest('hex')

				await this.channel.assertExchange(this.settings.exchange, this.settings.exchangeType, {
					durable: true,
					arguments:  args
				});
				
				await this.channel.publish(this.settings.exchange, event, Buffer.from(jsonData), {headers: headers});
			})()
		},

		async _event2Receive(event, listenerName, listener)
		{
			let handle = listener.handle
			let queue = await this._queueName(event, listenerName)
			var createDeadLetter = listener.createDeadLetter

			await this.channel.assertExchange(this.settings.exchange, this.settings.exchangeType, { durable: true });

			let args = {}

			if(listener.deduplication)
			{
				args = Object.assign(args, {
					'x-message-deduplication': true,
					'x-cache-size': 100,
					'x-cache-ttl': this.settings.deduplicationTTL,
					'x-cache-persistence': "disk"
				})
			}

			if(createDeadLetter)
			{
				args = Object.assign(args, {
					'x-dead-letter-exchange': this.settings.deadExchange,
					'x-dead-letter-routing-key': queue
				})
			}

			await this.channel.assertQueue(queue, { durable: true, arguments: args });
			this.channel.prefetch(1);

			this.channel.bindQueue(queue, this.settings.exchange, event);
			
			if(createDeadLetter)
            	this.channel.bindQueue(queue, this.settings.exchange, queue); //Dead letter -> to live queue
	
			this.channel.consume(queue, (message) => {
				let content = JSON.parse(message.content.toString())
				
				var channel = this.channel
				let ok = function(message) { channel.ack(message) }
				
				let fail = function(message) {
					console.log('NACK')

					if(createDeadLetter)
						channel.nack(message, false, false) //Dead letter
					else
						channel.ack(message)
				}

				handle.call(this, content, message, ok, fail)
			});
		},

		async deadLetter(event, listenerName)
		{
			let queue = await this._queueName(event, listenerName)
			let deadQueue = await this._deadQueueName(event, listenerName)
			await this.channel.assertExchange(this.settings.deadExchange, this.settings.exchangeType, { durable: true });

			let args = {
				'x-dead-letter-exchange': this.settings.exchange,
				'x-dead-letter-routing-key': queue,
				'x-message-ttl': this.settings.timeRetrive
			}

			await this.channel.assertQueue(deadQueue, { durable: true, arguments: args });
			this.channel.bindQueue(deadQueue, this.settings.deadExchange, queue);
		},

		async _queueName(event, listenerName)
		{
			return `evt2-${this.settings.serviceName}-${event}-${listenerName}`
		},

		async _deadQueueName(event, listenerName)
		{
			return `evt2-dead-${this.settings.serviceName}-${event}-${listenerName}`
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.broker.waitForServices([this.fullName]).then(async () => {
			this.settings.serviceName = this.schema.name

			this.events2 = this.schema.events2 || []
			this.settings.amqpUrl = this.settings.amqpUrl || this.broker.options.transporter
			this.connection = await amqp.connect(this.settings.amqpUrl)

			this.channel = await this.connection.createChannel()

			for(let event2 of this.events2)
			{
				let eventName = event2.event

				if(!eventName){
					console.log(`event2: event '${event2}' in service '${this.settings.serviceName}' invalid ignoring...`)
					continue
				} else if(!event2.listeners){
					console.log(`event2: event '${eventName}' in service '${this.settings.serviceName}' invalid ignoring...`)
					continue
				}

				for(let listenerName of Object.keys(event2.listeners))
				{
					let listener = Object.assign({ createDeadLetter: false, deduplication: true, handle: null}, event2.listeners[listenerName])

					let handle = listener.handle
					
					if(typeof handle !== 'function')
					{
						console.log(`event2: event '${eventName}' in service '${this.settings.serviceName}' has invalid handle ignoring...`)
						continue
					}
					
					if(listener.createDeadLetter)
						await this.deadLetter(eventName, listenerName)

					await this._event2Receive(eventName, listenerName, listener)
				}
			}
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};
