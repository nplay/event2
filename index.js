/*
 * event2
 * Copyright (c) 2020 Deogenes (https://github.com/Deogenes/event2)
 * MIT Licensed
 */

"use strict";

module.exports = require("./src");