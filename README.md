![Moleculer logo](http://moleculer.services/images/banner.png)

[![Build Status](https://travis-ci.org/Deogenes/event2.svg?branch=master)](https://travis-ci.org/Deogenes/event2)
[![Coverage Status](https://coveralls.io/repos/github/Deogenes/event2/badge.svg?branch=master)](https://coveralls.io/github/Deogenes/event2?branch=master)
[![Known Vulnerabilities](https://snyk.io/test/github/Deogenes/event2/badge.svg)](https://snyk.io/test/github/Deogenes/event2)

# event2 [![NPM version](https://img.shields.io/npm/v/event2.svg)](https://www.npmjs.com/package/event2)

Events with AMQP Support

## Features

## Install
```
npm install event2 --save
```

## Usage


## Test
```
$ npm test
```

In development with watching

```
$ npm run ci
```

## Contribution
Please send pull requests improving the usage and fixing bugs, improving documentation and providing better examples, or providing some testing, because these things are important.

## License
The project is available under the [MIT license](https://tldrlegal.com/license/mit-license).

## Contact
Copyright (c) 2020 Deogenes

[![@MoleculerJS](https://img.shields.io/badge/github-moleculerjs-green.svg)](https://github.com/moleculerjs) [![@MoleculerJS](https://img.shields.io/badge/twitter-MoleculerJS-blue.svg)](https://twitter.com/MoleculerJS)
